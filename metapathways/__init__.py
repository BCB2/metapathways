"""
MetaPathways is a modular pipeline to build PGDBs from metagenomic sequences.
"""

from metapathways import _version as mp_version

name = "metapathways"
__version__ = mp_version.__version__
