"""This script runs the functional search via BLAST or FAST like
homology search tools on a set of ORFs
"""
__author__ = "Kishori M Konwar"
__copyright__ = "Copyright 2020, MetaPathways"
__maintainer__ = "Kishori M Konwar"
__status__ = "Release"

try:
    import traceback
    import sys
    import re
    import glob
    import os
    import pandas as pd

    from os import path, _exit, rename
    from optparse import OptionParser, OptionGroup

    from metapathways import sysutil as sysutils
    from metapathways import general_utils as gutils
    from metapathways import metapathways_utils as mputils
    from metapathways import sysutil as sysutils
    from metapathways import errorcodes as errormod
except:
    print(""" Could not load some user defined  module functions""")
    print(traceback.print_exc(10))
    sys.exit(3)

PATHDELIM = sysutils.pathDelim()

usage = sys.argv[0] + """ -i input -o output [algorithm dependent options]"""

def createParser():
    epilog = """This script is used for running a homology search algorithm such as BLAST or FAST
              on a set of query amino acid sequences against a target of  reference protein sequences.
              Currently it supports the BLASTP and FAST algorithm. Any other homology search algorithm
              can be added by first adding the new algorithm name in upper caseusing in to the
              choices parameter in the algorithm option of this script.
              The results are put in a tabular form in the folder blast_results, with individual files
               for each of the databases. The files are named as "<samplename>.<dbname>.<algorithm>out"
              In the case of large number of amino acid sequences, this step of the computation can be
               also done using multiple grids (to use batch processing system) """

    epilog = re.sub(r"\s+", " ", epilog)
    parser = OptionParser(usage=usage, epilog=epilog)

    # Input options
    parser.add_option(
        "--algorithm",
        dest="algorithm",
        default="BLAST",
        choices=["BLAST", "FAST"],
        help="the homology search algorithm",
    )

    blast_group = OptionGroup(parser, "BLAST parameters")

    blast_group.add_option(
        "--blast_query",
        dest="blast_query",
        default=None,
        help="Query amino acid sequences for BLASTP",
    )

    blast_group.add_option(
        "--blast_db",
        dest="blast_db",
        default=None,
        help="Target reference database sequenes for BLASTP",
    )

    blast_group.add_option(
        "--blast_out", dest="blast_out", default=None, help="BLAST output file"
    )

    blast_group.add_option(
        "--blast_outfmt",
        dest="blast_outfmt",
        default="6",
        help="BLASTP output format [default 6, tabular]",
    )

    blast_group.add_option(
        "--blast_evalue",
        dest="blast_evalue",
        default=None,
        help="The e-value cutoff for the BLASTP",
    )

    blast_group.add_option(
        "--num_threads",
        dest="num_threads",
        default="1",
        type="str",
        help="Number of BLAST threads",
    )

    blast_group.add_option(
        "--blast_max_target_seqs",
        dest="blast_max_target_seqs",
        default=None,
        help="Maximum number of target hits per query",
    )

    blast_group.add_option(
        "--blast_executable",
        dest="blast_executable",
        default=None,
        help="The BLASTP executable",
    )

    blast_group.add_option(
        "--num_hits",
        dest="num_hits",
        default="10",
        type="str",
        help="The BLASTP executable",
    )

    parser.add_option_group(blast_group)

    last_group = OptionGroup(parser, "FAST parameters")

    last_group.add_option(
        "--last_query",
        dest="last_query",
        default=None,
        help="Query amino acid sequences for FAST",
    )

    last_group.add_option(
        "--last_db",
        dest="last_db",
        default=None,
        help="Target reference database sequenes for FAST",
    )

    last_group.add_option(
        "--last_f",
        dest="last_f",
        default="0",
        help="FAST output format [default 0, tabular]",
    )

    last_group.add_option(
        "--last_o", dest="last_o", default=None, help="FAST output file"
    )

    last_group.add_option(
        "--last_executable",
        dest="last_executable",
        default=None,
        help="The FAST executable",
    )

    last_group.add_option(
        "--run_mode",
        dest="run_mode",
        default='default',
        help="Run large DBs normally (default) or per volume [pervol]",
    )

    parser.add_option_group(last_group)

    return parser


def main(argv, errorlogger=None, runcommand=None, runstatslogger=None):
    parser = createParser()
    options, args = parser.parse_args(argv)

    if options.algorithm == "BLAST":
        (code, message) = _execute_BLAST(options, logger=errorlogger)

    elif options.algorithm == "FAST":
        (code, message) = _execute_FAST(options, logger=errorlogger)
    else:
        gutils.eprintf("ERROR\tUnrecognized algorithm name for FUNC_SEARCH\n")
        if errorlogger:
            errorlogger.printf("ERROR\tUnrecognized algorithm name for FUNC_SEARCH\n")
        # exit_process("ERROR\tUnrecognized algorithm name for FUNC_SEARCH\n")
        return -1

    if code != 0:
        a = "\nERROR\tCannot successfully execute the %s for FUNC_SEARCH\n" % (
            options.algorithm
        )
        b = "ERROR\t%s\n" % (message)
        c = "INFO\tDatabase you are searching against may not be formatted correctly (if it was formatted for an earlier version) \n"
        code = -1
        d = "INFO\tTry removing the files for that database in 'formatted' subfolder for MetaPathways to trigger reformatting \n"
        if options.algorithm == "BLAST":
            e = "INFO\tYou can remove as 'rm %s.*','\n" % (options.blast_db)
        if options.algorithm == "FAST":
            e = "INFO\tYou can remove as 'rm %s.*','\n" % (options.last_db)

        (code, message) = _execute_FAST(options, logger=errorlogger)
        f = "INFO\tIf removing the files did not work then format it manually (see manual)"
        outputStr = a + b + c + d + e + f

        gutils.eprintf(outputStr + "\n")

        if errorlogger:
            errorlogger.printf(outputStr + "\n")
        return code

    return 0


def _execute_FAST(options, logger=None):
    
    volumes = 0
    if options.run_mode == 'pervol':
        # open *.prj file to check if there are multiple volumes
        # if there are then use the per-volume function
        with open(options.last_db + '.prj', 'r') as prj_in:
            dat_rl = prj_in.readlines()
            for line in dat_rl:
                if 'volumes=' in line:
                    volumes = int(line.split('=')[1].strip('\n'))

    # create argument list(s), depending on the number of volumes
    args_list = []
    if volumes > 0:
        for v in list(range(volumes)):
            args = []
            args.append(options.last_executable)
            args += ["-f", options.last_f]
            args += ["-o", options.last_o + str(v) + ".tmp"]
            args += ["-P", options.num_threads]
            args += [" -K", options.num_hits]
            args += [options.last_db + str(v)]
            args += [options.last_query]   
            args_list.append(args)
    else: # if only one volume OR running in default mode
        args = []
        args.append(options.last_executable)
        args += ["-f", options.last_f]
        args += ["-o", options.last_o + ".tmp"]
        args += ["-P", options.num_threads]
        args += [" -K", options.num_hits]
        args += [options.last_db]
        args += [options.last_query]
        args_list.append(args)

    result = None
    try:
        if len(args_list) == 1:
            a = args_list[0]
            result = sysutils.getstatusoutput(" ".join(a))
            rename(a[4], a[4].rsplit('.', 1)[0])
        else:
            for a in args_list:
                result = sysutils.getstatusoutput(" ".join(a))
                rename(a[4], a[4].rsplit('.', 1)[0])
            out_list = glob.glob(options.last_o + '*')
            with open(options.last_o, 'w') as outfile:
                for fname in out_list:
                    with open(fname) as infile:
                        for line in infile:
                            outfile.write(line)
                    os.remove(fname)
            # sort the final table on ORF and Bitscore
            last_df = pd.read_csv(options.last_o, sep='\t', header=None)
            last_df.sort_values(by = [0, 11], ascending = [True, False], inplace=True)
            last_df.to_csv(options.last_o, sep='\t', header=False, index=False)
    except:
        message = "Could not run FAST correctly"
        if result and len(result) > 1:
            message = result[1]
        if logger:
            logger.printf("ERROR\t%s\n", message)
        return (1, message)

    return (result[0], result[1])


def _execute_BLAST(options, logger=None):
    args = []

    if options.blast_executable:
        args.append(options.blast_executable)

    if options.blast_max_target_seqs:
        args += ["-max_target_seqs", options.blast_max_target_seqs]

    if options.num_threads:
        args += ["-num_threads", options.num_threads]

    if options.blast_outfmt:
        args += ["-outfmt", options.blast_outfmt]

    if options.blast_db:
        args += ["-db", options.blast_db]

    if options.blast_query:
        args += ["-query", options.blast_query]

    if options.blast_evalue:
        args += ["-evalue", options.blast_evalue]

    if options.blast_out:
        args += ["-out", options.blast_out + ".tmp"]

    result = sysutils.getstatusoutput(" ".join(args))
    rename(options.blast_out + ".tmp", options.blast_out)
    return (result[0], result[1])

def MetaPathways_func_search(
    argv, extra_command=None, errorlogger=None, runstatslogger=None):

    if errorlogger != None:
        errorlogger.write("#STEP\tFUNC_SEARCH\n")
    try:
         main(
             argv,
             errorlogger = errorlogger,
             runcommand = extra_command,
             runstatslogger = runstatslogger,
            )
    except:
        errormod.insert_error(4)
        return (1, traceback.print_exc(10))

    return (0, "")

if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1:])
