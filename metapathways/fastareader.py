"""This module defines classes for working with Fasta records."""

__author__ = "Kishori M Konwar"
__copyright__ = "Copyright 2020, MetaPathways"
__maintainer__ = "Kishori M Konwar"
__status__ = "Release"

try:
    import traceback
    import re
    import sys
    import gzip
except:
    print(""" Could not load some user defined  module functions""")
    print(traceback.print_exc(10))
    sys.exit(3)

class FastaRecord:
    def __init__(self, longname, sequence):
        self.longname = longname
        self.sequence = sequence
        fields = [x.strip() for x in self.longname.split(" ")]
        if len(fields) > 0:
            self.name = fields[0]
        else:
            self.name = None


class FastaReader:
    """Parses a fasta record from a string or file."""

    stop = False
    START_PATTERN = re.compile(r"^>")
    name = None
    future_name = None
    sequence = ""

    def __init__(self, fasta_filename):
        try:
            if fasta_filename.endswith((".gz", "_files")):
                self.file = gzip.open(fasta_filename,'rt')
            else:
                self.file = open(fasta_filename, "r")
        except IOError:
            print("Cannot open fasta file " + fasta_filename)

    def __iter__(self):
        return self

    def __next__(self):
        if self.stop:
            raise StopIteration

        try:
            if not self.name:
                self.name = self.file.readline().strip()
            line = self.file.readline()
        except:
            line = None

        if not line:
            self.stop = True
            raise StopIteration

        fragments = []
        while line and not self.START_PATTERN.search(line):
            fragments.append(line.strip())
            line = self.file.readline()

        # print line
        if self.future_name:
            self.name = self.future_name

        if line:
            self.future_name = line.strip()

        self.sequence = "".join(fragments)
        self.seqname = self.name

        return FastaRecord(self.name, self.sequence)
