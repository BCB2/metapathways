"""Scans the contigs for rRNA genes by BLASTing the preprocessed input
against the taxonomy databases such as silva or greenegenes
"""

__author__ = "Kishori M Konwar"
__copyright__ = "Copyright 2020, MetaPathways"
__maintainer__ = "Kishori M Konwar"
__status__ = "Release"

try:
    import traceback
    import optparse
    import sys
    import re

    from os import path

    from metapathways import sysutil as sysutils
    from metapathways import metapathways_utils as mputils
    from metapathways import general_utils as gutils
    from metapathways import sysutil as sysutils
    from metapathways import errorcodes as errormod
except:
    print(""" Could not load some user defined  module functions""")
    print(traceback.print_exc(10))
    sys.exit(3)

PATHDELIM = sysutils.pathDelim()

# sequences with no seperate taxonomic name gets the sequences name
def parse_Format_0(line):
    fields = re.split(" ", line)
    if len(fields) == 1:
        name = fields[0].replace(">", "")
        taxonomy = name
    else:
        return (None, None)
    return (name.strip(), taxonomy.strip())


# silva
def parse_Format_1(line):
    fields = re.split(" ", line)
    if len(fields) >= 2:
        name = fields[0].replace(">", "")
        fields = fields[1:]
        taxonomy = " "
        taxonomy = taxonomy.join(fields)
    else:
        return (None, None)

    return (name.strip(), taxonomy.strip())


def parse_Format_2(line):
    fields = re.split(" ", line)
    names = []
    if len(fields) >= 2:
        name = fields[0].replace(">", "")
        for field in fields[1:]:
            field = field.strip()
            if re.match("[a-z]_[a-zA-Z0-9-_]*;", field):
                names.append(field)
        taxonomy = ""
        taxonomy = taxonomy.join(names)
    else:
        return (None, None)

    return (name.strip(), taxonomy.strip())


def getName_and_Taxonomy(line, format=0):

    if format == 0:
        name, taxonomy = parse_Format_0(line)
    elif format == 1:
        name, taxonomy = parse_Format_1(line)
    #   print name + "  " + taxonomy
    elif format == 2:
        (name, taxonomy) = parse_Format_2(line)
    else:
        return (None, None)

    return (name, taxonomy)


START_PATTERN = re.compile(r"^>")

def read_select_fasta_sequences(candidates, records, input_file_name):
    input_file = open(input_file_name, "r")
    line = input_file.readline()
    while line:
        if START_PATTERN.search(line):
            name = line.strip()
            name = re.sub(r">", "", name)
            if name and name in candidates:
                records[name] = ""
        else:
            sequence = line.strip()
            if sequence and name in candidates:
                records[name] += sequence
        line = input_file.readline()

    input_file.close()


def write_selected_sequences(selected_sequences, output_file_name):
    output_file = open(output_file_name, "w")
    for read in selected_sequences:
        gutils.fprintf(output_file, ">%s\n", read)
        gutils.fprintf(output_file, "%s\n", selected_sequences[read])

    output_file.close()


def append_taxonomic_information(databaseSequences, table, params):
    with open(databaseSequences, "r") as tax_seqs:
        format = gutils.getFormat(databaseSequences)
        taxmapLines = tax_seqs.readlines()
        tax_seqs.close()
    
        taxMapping = {}
        for line in taxmapLines:
            if not re.match(">", line):
                continue
            line = line.strip()
            name, taxonomy = getName_and_Taxonomy(line, format)
            if name:
                taxMapping[name] = taxonomy
    
        for key in table:
            key = str(key)
            if (
                int(table[key][5] - table[key][4]) > params["length"]
                and table[key][0] > params["similarity"]
                and table[key][1] < params["evalue"]
                and table[key][2] > params["bitscore"]
            ):
                if table[key][3] in taxMapping:
                    table[key].append(taxMapping[table[key][3]])
                else:
                    table[key].append("-")
            else:
                table[key].append("-")


def process_blastout_file(blast_file, database, table, subunit, query_fna, errorlogger=None):
    try:
        blastfile = open(blast_file, "r")
    except IOError:
        gutils.eprintf("ERROR : Cannot write read file " + blast_file + " !")
        if errorlogger != None:
            errorlogger.write(
                "STATS_rRNA\tERROR\tCannot write read blast output file "
                + blast_file
                + " for database "
                + database
            )
        mputils.exit_process()

    blastLines = blastfile.readlines()
    blastfile.close()
    try:
        queryseqs = open(query_fna, "r")
    except IOError:
        gutils.eprintf("ERROR : Cannot write read file " + query_fna + " !")
        if errorlogger != None:
            errorlogger.write(
                "STATS_rRNA\tERROR\tCannot find read query sequences "
                + query_fna
            )
        mputils.exit_process()

    queryDict = {x[1:].split(' ', 1)[0].strip('\n') : x[1:].replace(' ', '_').strip('\n')
                 for x in queryseqs.readlines() if x[0] == '>'
                 }
    queryseqs.close()

    for line in blastLines:
        line = line.strip()
        fields = re.split("\t", line)
        if len(fields) < 12:
            continue
        query_key = str(fields[0].strip())
        if query_key in queryDict:
            query_id = queryDict[str(fields[0].strip())]
            target_id = str(fields[1].strip())
            percent_id = float(fields[2].strip())
            start_pos = int(fields[6].strip())
            end_pos = int(fields[7].strip())
            e_value = float(fields[10].strip())
            bitscore = float(fields[11].strip())
            length = end_pos - start_pos + 1
            if subunit in query_id: # check subunit
                if query_key in table: # compare blast stats for queries
                    t_percent_id = table[query_key][0]
                    t_e_value = table[query_key][1]
                    t_bitscore = table[query_key][2]
                    t_length = table[query_key][5] - table[query_key][4] + 1
                    if ((bitscore >= t_bitscore) &
                        (length > t_length)): # only replace if bitscore is better AND alignment is longer
                        table[query_key] = [percent_id,
                                        e_value,
                                        bitscore,
                                        target_id,
                                        start_pos,
                                        end_pos,
                                        ]

                else: # not in table yet
                    table[query_key] = [percent_id,
                                        e_value,
                                        bitscore,
                                        target_id,
                                        start_pos,
                                        end_pos,
                                        ]


usage = (
    sys.argv[0]
    + """ -i x.blastout [y.blastout] -d  xdatabase [ydatabase]  -m xtax_maps [ ytax_maps ] -o outputfile -b n -e 0.ddd -s N """
)

def createParser():

    epilog = """The input nucleotide sequences are BLASTed against the selected rRNA databases such as  SSU or LSU Silva sequence databases and SSU Greengene database. The hits with high bit scores are flagged as rRNA and the resulting taxonomy from the databases are assigned.  The results from this step are put in the results/rRNA folder, with one tsv file for each rRNA database."""
    epilog = re.sub(r"\s+", " ", epilog)

    parser = optparse.OptionParser(usage=usage, epilog=epilog)

    # Input options

    input_group = optparse.OptionGroup(parser, "input options")

    input_group.add_option(
        "-i",
        "--blastout",
        dest="blast_files",
        metavar="BLAST_FILE",
        action="append",
        default=[],
        help="BLAST  output file  from blasting of nucleotide sequeces against Taxonomic databases",
    )

    input_group.add_option(
        "-d",
        "--databases",
        dest="tax_databases",
        metavar="TAX_DATABASE",
        action="append",
        default=[],
        help="Taxonomic databases",
    )

    input_group.add_option(
        "-r",
        "--subunit",
        dest="subunit",
        metavar="subunit",
        default='16S',
        help="rRNA subunit [16S or 23S]",
    )

    input_group.add_option(
        "-o", "--output", dest="output", metavar="OUTPUTE", help="Taxonomic databases"
    )

    input_group.add_option(
        "-f",
        "--fasta",
        dest="fasta",
        metavar="NUC_SEQUENCES",
        help="Output select nucleotide sequences",
    )

    input_group.add_option(
        "-q",
        "--query",
        dest="query",
        metavar="NUC_SEQUENCES",
        help="Query nucleotide sequences",
    )

    parser.add_option_group(input_group)

    # filtering options
    filtering_options = optparse.OptionGroup(parser, "filteroptions")

    filtering_options.add_option(
        "-b",
        "--bit_score",
        dest="bitscore",
        metavar="BITSCORE",
        default=0,
        help="bit score cutoff",
    )

    filtering_options.add_option(
        "-e",
        "--e-value",
        metavar="evalue",
        dest="evalue",
        default=1e-6,
        help="e-value cut off for the blast hits",
    )

    filtering_options.add_option(
        "-s",
        "--similarity",
        metavar="similarity",
        dest="similarity",
        default=40,
        help="% similarity cut off for the blast hits",
    )

    filtering_options.add_option(
        "-l",
        "--length",
        metavar="length",
        dest="length",
        default=180,
        help="length cut off for the blast hits",
    )

    parser.add_option_group(filtering_options)

    # run process  parameters
    process_options = optparse.OptionGroup(parser, "processoptions")

    process_options.add_option(
        "-n",
        "--num_threads",
        dest="num_threads",
        metavar="num_threads",
        default=1,
        help="number of threads",
    )

    parser.add_option_group(process_options)

    return parser


def main(argv, errorlogger=None, runcommand=None, runstatslogger=None):
    parser = createParser()
    options, args = parser.parse_args(argv)

    if not len(options.blast_files):
        parser.error("At least one taxonomic BLAST output is required")

    if runBlastCommandrRNA(runcommand=runcommand) != 0:
        if errorlogger:
            errorlogger.write(
                "ERROR: Failed to BLAST the sequences against database %s : "
                % (options.tax_databases[0])
            )
            errorlogger.write("     : " + runcommand)
        mputils.exit_process(
            "ERROR: Failed to BLAST the sequences against database %s : "
            % (options.tax_databases[0])
            + "     : "
            + runcommand
        )

    if not (len(options.tax_databases) == len(options.blast_files)):
        parser.error(
            "Number of taxonomic databases and BLAST outputs should be the same"
        )

    if not options.output:
        parser.error("Output file must be specified")
    # Incredible sanity check

    if not gutils.files_exist(options.blast_files):
        sys.exit(1)

    if not gutils.files_exist(options.tax_databases):
        sys.exit(1)

    params = {
        "length": int(options.length),
        "similarity": float(options.similarity),
        "evalue": float(options.evalue),
        "bitscore": float(options.bitscore),
    }
    # print params['bitscore']
    table = {}
    for x in range(0, len(options.blast_files)):
        table[options.tax_databases[x]] = {}
        process_blastout_file(
            options.blast_files[x],
            options.tax_databases[x],
            table[options.tax_databases[x]],
            options.subunit,
            options.query,
            errorlogger=errorlogger,
        )

    priority = 7000
    reads = {}
    for x in range(0, len(options.blast_files)):
        append_taxonomic_information(
            options.tax_databases[x], table[options.tax_databases[x]], params
        )
        for key in table[options.tax_databases[x]]:
            if len(table[options.tax_databases[x]][key][6]) > 1:
                reads[key] = True

        dbname = re.sub(r"^.*" + PATHDELIM, "", options.tax_databases[x])
        if runstatslogger:
            runstatslogger.write(
              "%s\tTaxonomic hits in %s\t%s\n" % (str(priority), dbname, str(len(reads))))
        priority += 1
    outputfile = open(options.output, "w")
    gutils.fprintf(outputfile, "#Similarity cutoff :\t" + str(params["similarity"]) + "\n")
    gutils.fprintf(outputfile, "#Length cutoff :\t" + str(params["length"]) + "\n")
    gutils.fprintf(outputfile, "#Evalue cutoff :\t" + str(params["evalue"]) + "\n")
    gutils.fprintf(outputfile, "#Bit score cutoff :\t" + str(params["bitscore"]) + "\n")
    gutils.fprintf(
        outputfile, "#Number of rRNA sequences detected:\t" + str(len(reads)) + "\n\n"
    )

    for x in range(0, len(options.tax_databases)):
        #  printf('\t%s\t\t\t', re.sub(r'^.*/','', options.tax_databases[x]))
        gutils.fprintf(
            outputfile,
            "\t%s\t\t\t",
            re.sub(r"^.*" + PATHDELIM, "", options.tax_databases[x]),
        )
    # printf('\n')
    gutils.fprintf(outputfile, "\n")

    # printf('%s', 'read')
    for x in range(0, len(options.blast_files)):
        gutils.fprintf(
            outputfile,
            "%s\t%s\t%s\t%s\t%s\t%s\t%s",
            "sequence",
            "start",
            "end",
            "similarity",
            "evalue",
            "bitscore",
            "taxonomy",
        )
    gutils.fprintf(outputfile, "\n")

    for read in reads:
        # printf('%s', read)
        gutils.fprintf(outputfile, "%s", read)
        for x in range(0, len(options.blast_files)):
            if read in table[options.tax_databases[x]]:
                gutils.fprintf(
                    outputfile,
                    "\t%s\t%s\t%s\t%s\t%s\t%s",
                    str(table[options.tax_databases[x]][read][4]),
                    str(table[options.tax_databases[x]][read][5]),
                    str(table[options.tax_databases[x]][read][0]),
                    str(table[options.tax_databases[x]][read][1]),
                    str(table[options.tax_databases[x]][read][2]),
                    str(table[options.tax_databases[x]][read][6]),
                )
            else:
                gutils.fprintf(outputfile, "\t-\t-\t-\t-\t-\t-")
        gutils.fprintf(outputfile, "\n")
    outputfile.close()

    # collect the exact reads
    database_hits = {}
    for read in reads:
        for x in range(0, len(options.blast_files)):
            if read in table[options.tax_databases[x]]:
                database_hits[read] = [
                    table[options.tax_databases[x]][read][4],
                    table[options.tax_databases[x]][read][5],
                ]

    # pick the hits, trim them according to the match and write them
    if options.fasta:
        selected_sequences = {}
        read_select_fasta_sequences(database_hits, selected_sequences, options.fasta)
        for read in database_hits:
            selected_sequences[read] = selected_sequences[read][
                database_hits[read][0] : database_hits[read][1]
            ]
        write_selected_sequences(selected_sequences, options.output + ".fasta")


def runBlastCommandrRNA(runcommand=None):
    if runcommand == None:
        return False
    result = sysutils.getstatusoutput(runcommand)

    return result[0]


def MetaPathways_rRNA_stats_calculator(
    argv, extra_command=None, errorlogger=None, runstatslogger=None):
    if errorlogger != None:
        errorlogger.write("#STEP\tSTATS_rRNA\n")
    try:
        main(
            argv,
            errorlogger=errorlogger,
            runcommand=extra_command,
            runstatslogger=runstatslogger,
        )
    except:
        errormod.insert_error(6)
        return (1, traceback.print_exc(10))

    return (0, "")

if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1:])
