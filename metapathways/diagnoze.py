"""Checks the required tools for running the pipeline"""

__author__ = "Kishori M Konwar"
__copyright__ = "Copyright 2020, MetaPathways"
__maintainer__ = "Kishori M Konwar"
__status__ = "Release"

"""Contains general utility code for the metapaths project"""

try:
    import traceback
    import sys
    import glob
    import shutil
    import re

    from shutil import rmtree
    from optparse import make_option
    from os import path, _exit, rename, remove

    from metapathways import parameters as paramsmod
    from metapathways import configuration as configmod
    from metapathways import sysutil as sysutils
    from metapathways import general_utils as gutils
    from metapathways import errorcodes as errormod
except:
    print("Cannot load some modules")
    print(traceback.print_exc(10))
    sys.exit(0)

PATHDELIM = sysutils.pathDelim()


def staticDiagnose(params, config = None, logger=None):
    """
    Diagnozes the pipeline basedon the configs and params for
    binaries, scripts and resources
    """

    """ makes sure that the choices in  parameter file are valid """
    errors = checkParams(params, logger=logger)
    if errors:
        return False

    """ Get the configurations for the executables/scripts and databasess """
    _configuration = configmod.Configuration()
    configuration = _configuration.getConfiguration()

    """ the place holders for the tools required to make the run """
    parameters = paramsmod.Parameters()

    """ check if the required standard databases exists """

    #    print  parameters.getRunSteps( activeOnly = True)
    if not checkForRequiredDatabases(
        params, config, "functional", logger=logger):
        errormod.insert_error(17)
        return False

    if not checkForRequiredDatabases(
        params, config, "taxonomic", logger=logger):
        errormod.insert_error(17)
        return False

    """ make sure all the executables exist """
    executables = [ 'fastal', 'fastdb', 'coverm' ]
    message, ok = checkbinaries(executables)

    if message:
        print(message)
        return False

    return True


def checkbinaries(executables):
    message = ""
    ok = True
    cmd_exists = lambda x: shutil.which(x) is not None
    for executable in executables:
        if not cmd_exists(executable):
            message = "'" + executable + "'" + " is missing.\n"
            ok = False

    return message, ok

def checkForRequiredDatabases(params, config,  dbType, logger=None):
    """checks the
    -- database folder structure
    -- checks for raw sequences
    -- checks for formatted sequences
    -- formats if necessary
    """

    if dbType == "functional":
        dbs = get_parameter(params, "Functional Annotation Arguments", "annotation_dbs", default=None)
        _algorithm = get_parameter(params, "Functional Annotation Arguments", "annotation_algorithm", default=None)

    if dbType == "taxonomic":
        dbs = get_parameter(params, "rRNA Annotation Arguments", "rRNA_refdbs", default=None)
        _algorithm = 'BLAST' #get_parameter(params, "annotation", "algorithm", default=None)

    if dbs == None:
        gutils.eprintf(
            "WARNING\tReference databases to annotate with is unspecified, please add it in the params file\n"
        )
        return False

    if not dbs:
        return True

    """ checks refdb path """
    if not check_if_refDB_path_valid(config.refdb_dir, logger=logger):
        return False

    """ checks raw sequences for dbtype functional/taxonimic """
    if isRefDBNecessary(params, dbType):
        if not check_for_raw_sequences(dbs, config.refdb_dir, dbType, logger=logger):
            errormod.insert_error(17)
            return False

        for db in dbs:
            algorithm = ""
            if dbType == "taxonomic":
                algorithm = 'BLAST'
                seqType = "nucl"
            elif dbType == "functional":
                algorithm = _algorithm
                seqType = "prot"
            else:
                algorithm = None

            """ is db formatted ? """
            if not isDBformatted(db, config.refdb_dir, dbType, seqType, \
                algorithm, logger=logger
                ):
                """ if note formatted then format it """
                gutils.eprintf(
                    "WARNING\tFirst format %s database %s\n", seqType, gutils.sQuote(db)
                )
                logger.printf(
                    "WARNING\tFirst format %s database %s \n", seqType, gutils.sQuote(db)
                )

                return False


            """ check for dbMapFile """
            dbMapFile = config.refdb_dir + PATHDELIM + dbType + PATHDELIM + "formatted" + PATHDELIM + db + "-names.txt"
            if not gutils.doesFileExist(dbMapFile):
                gutils.eprintf(
                    "WARNING\tCreate the db annotation file %s for %s\n",
                    gutils.sQuote(dbMapFile),
                    gutils.sQuote(db),
                )
                logger.printf(
                    "WARNING\tCreate the db annotation file %s for %s\n",
                    gutils.sQuote(dbMapFile),
                    gutils.sQuote(db),
                )

    return True


def isRefDBNecessary(params, dbType):
    """ decide yes or no based on the params settings yes or redo """
    if dbType == "functional":
        status = get_parameter(params, "metapaths_steps", "FUNC_SEARCH", default=None)
        if status in ["yes", "redo"]:
            return True

    if dbType == "taxonomic":
        status = get_parameter(params, "metapaths_steps", "SCAN_rRNA", default=None)
        if status in ["yes", "redo"]:
            return True

    return False


def isDBformatted(db, refdbspath, dbType, seqType, algorithm, logger=None):
    """ check if the DB is formatted """
    """Checks if the formatted database for the specified algorithm exits """
    dbPath = refdbspath + PATHDELIM + dbType + PATHDELIM + "formatted"
    dbname = dbPath + PATHDELIM + db
    suffixes = getSuffixes(algorithm, seqType)

    # print algorithm, suffixes
    if not suffixes:
        return False

    status = False
    for suffix in suffixes:
        allfileList = glob.glob(dbname + "*." + suffix)

        fileList = []
        tempFilePattern = re.compile(r"" + dbname + "[.\d]*." + suffix + "$")

        for aFile in allfileList:
            searchResult = tempFilePattern.search(aFile)
            if searchResult:
                fileList.append(aFile)

        if len(fileList) == 0:
            gutils.eprintf("WARNING\tsequence for db  %s not formatted\n", dbname)
            logger.printf("WARNING\tsequence for db  %s not formatted\n", dbname)
            return False

        status = True

    return status


def check_if_refDB_path_valid(refdbspath, logger=None):
    """it checks for the validity of the refdbs path structure
    refdbpath  /functional
                   /formatted
               /tanxonomic
                   /formatted
    """

    status = True
    if not gutils.doesFolderExist(refdbspath):
        gutils.eprintf("ERROR\treference sequence folder %s not found\n", gutils.sQuote(refdbspath))
        logger.printf(
            "ERROR\treference sequence folder %s not found\n", gutils.sQuote(refdbspath)
        )
        return False

    dbTypes = ["functional", "taxonomic"]
    """ now check if respective dbtype folders are available """
    status = True
    for dbType in dbTypes:
        if not gutils.doesFolderExist(refdbspath + PATHDELIM + dbType):
            gutils.eprintf(
                "ERROR\tfolder %s for reference type %s not found\n",
                gutils.sQuote(refdbspath + PATHDELIM + dbType),
                dbType,
            )
            logger.printf(
                "ERROR\tfolder %s for reference type %s not found\n",
                gutils.sQuote(refdbspath + PATHDELIM + dbType),
                dbType,
            )
            status = False

    if status == False:
        return status

    """ now check if path to drop the formatted dbs are available """
    for dbType in dbTypes:
        if not gutils.doesFolderExist(
            refdbspath + PATHDELIM + dbType + PATHDELIM + "formatted"
        ):
            gutils.eprintf(
                "ERROR\tsubfolder %s not found under the folder %s\n",
                gutils.sQuote("formatted"),
                gutils.sQuote(refdbspath + PATHDELIM + dbType + PATHDELIM),
            )
            logger.printf(
                "ERROR\tsubfolder %s not found under the folder %s\n",
                gutils.sQuote("formatted"),
                gutils.sQuote(refdbspath + PATHDELIM + dbType + PATHDELIM),
            )
            status = False

    return status


def check_for_raw_sequences(dbs, refdbspath, dbType, logger=None):
    """ check for the raw sequence file """
    status = True
    for db in dbs:
        fullPath = refdbspath + PATHDELIM + dbType + PATHDELIM + db
        if not gutils.plain_or_gz_file_exists(fullPath):
            gutils.eprintf(
                "ERROR\tRaw sequences %s expected for %s references\n", fullPath, dbType
            )
            logger.printf(
                "ERROR\tRaw sequences %s expected for %s references\n", fullPath, dbType
            )
            status = False
    return status


def get_parameter(params, category, field, default=None):
    """gets the parameter value from a category
    as specified in the  parameter file"""

    if params == None:
        return default

    if category in params:
        if field in params[category]:
            return params[category][field]
        else:
            return default
    return default


def _checkParams(params, paramsAccept, logger=None, errors=None):

    """make sure that every parameter in the params is valid recursively
    This is initialed by the checkParams() function
    store the erros in the erros dictionary
    """
    """ if not level to go deeper  then the leaves of the dict are reached"""

    if not type(params) is dict and type(paramsAccept) is dict:
        # print  'type ',  params, paramsAccept,  (not params in paramsAccept), (len(paramsAccept.keys())!=0)
        try:
            if (not params in paramsAccept) and len(paramsAccept.keys())!=0:
                errors[params] = False
                choices = ", ".join(paramsAccept.keys())
                gutils.eprintf(
                    "ERROR\tValue for key %s, in param file, is not set propertly must be one of %s\t%s\n",
                    gutils.sQuote(params),
                    gutils.sQuote(choices),
                    __name__,
                )
                logger.printf(
                    "ERROR\tValue for key %s, in param file, is not set propertly must be one of %s\t%s\n",
                    gutils.sQuote(params),
                    gutils.sQuote(choices),
                    __name__,
                )
        except:
            pass
        return

    """  make sure that every parameter in the params is valid recursively """
    for key, value in params.items():
        if type(paramsAccept) is dict:
            if len(key) and key in paramsAccept:
                _checkParams(
                    params[key], paramsAccept[key], logger=logger, errors=errors
                )


def checkParams(params, logger=None):
    """ makes sure that all the params provides are valid or acceptable """
    """ when the choices are not any of the acceptable
    values then it is considered erroneous"""

    _paramsAccept = paramsmod.Parameters()
    paramsAccept = _paramsAccept.getAcceptableParameters()
    errors = {}

    for key, value in params.items():
        if key in paramsAccept:
            _checkParams(params[key], paramsAccept[key], logger=logger, errors=errors)

    return errors


def getSuffixes(algorithm, seqType):
    """Get the suffixes for the right algorithm with the right
    sequence type
    """

    suffixes = {}
    suffixes["FAST"] = {}
    suffixes["BLAST"] = {}
    suffixes["BLAST"]["nucl"] = ["nhr", "nsq", "nin"]
    suffixes["BLAST"]["prot"] = ["phr", "psq", "pin"]

    suffixes["FAST"]["nucl"] = ["des", "sds", "suf", "bck", "prj", "ssp", "tis"]
    suffixes["FAST"]["prot"] = ["des", "sds", "suf", "bck", "prj", "ssp", "tis"]

    if not algorithm in suffixes:
        return None

    if not seqType in suffixes[algorithm]:
        return None

    return suffixes[algorithm][seqType]
