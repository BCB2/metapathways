version: 2.1

orbs:
  python: circleci/python@1.3.2
  # python: rohanpm/python@1.3.0
  codecov: codecov/codecov@1.1.3
  bits-python: broadinstitute/bits-python@0.2.0

executors:

  python36:
    description: An environment with Python 3.6, pip & virtualenv
    docker:
    - image: circleci/python:3.6.8-stretch
    environment:
      CIRCLECI_PYTHON_EXECUTOR: python36

  python37:
    description: An environment with Python 3.7, pip & virtualenv
    docker:
    - image: circleci/python:3.7.2-stretch
    environment:
      CIRCLECI_PYTHON_EXECUTOR: python37

  python38:
    description: An environment with Python 3.8, pip & virtualenv
    docker:
    - image: circleci/python:3.8.5-buster
    environment:
      CIRCLECI_PYTHON_EXECUTOR: python38

commands:

  tox:
    description: |-
      Run one or more tox environments.
    parameters:
      toxenv:
        description: tox environment name, or multiple comma-separated names.
        type: string
    steps:
      - run:
          name: Run tox
          command: tox -e << parameters.toxenv >>

jobs:
  lint:
    parameters:
      executor:
        description: Executor used for linting; use this to select major Python version.
        type: executor
        default: python38
    executor: << parameters.executor >>
    steps:
      - checkout
      - run:
          name: install flake8
          command: pip install flake8
      - run:
          name: run flake8
          command: flake8 .

  build:
    parameters:
      executor:
        description: Executor used for building MetaPathways; use this to select major Python version.
        type: executor
        default: python38
      cache-version:
        default: v1
        description: >-
          Change the default cache version if you need to clear the cache for any
          reason.
        type: string
      py-version:
        type: string
        default: '3.8'
    executor: << parameters.executor >>
    steps:
      - checkout
      - python/dist
      - save_cache:
          key: >-
            cache-<< parameters.cache-version >>-pip-<< parameters.py-version >>-{{ checksum "requirements.txt" }}
          paths:
            - ~/.cache/pip

  test:
    description: |-
      Test a project using tox:

      - checkout sources
      - install tox
      - run tox environment(s)
      - store test results (optional)
      - persist coverage data (optional)
    parameters:
      executor:
        description: Executor used for this tox run; use this to select major Python version.
        type: executor
        default: python38
      toxenv:
        description: 'tox environment name, or multiple comma-separated names.'
        type: string
      persist_coverage:
        description: |-
          If true, any .coverage data produced by the tox run will be persisted
          to workspace, for use by one of the coverage-submitting jobs provided
          by this orb.

          The layout of the persisted coverage files is not defined and is intended
          for use only with the coverage-submitting jobs within this orb.
        type: boolean
        default: false
    executor: << parameters.executor >>
    steps:
      - checkout
      - run:
          command: |
            pip install tox
      - tox:
          toxenv: << parameters.toxenv >>
      - when:
          condition: << parameters.persist_coverage >>
          steps:
            - run:
                name: Prepare coverage file
                command: |-
                  if test -f .coverage; then
                    mv -v .coverage coverage.${CIRCLECI_PYTHON_EXECUTOR}.<< parameters.toxenv >>;
                  fi
            - persist_to_workspace:
                root: .
                paths:
                  - coverage.*
      - save_cache:
          key: >-
            v1-cache-pip-<< parameters.toxenv >>-{{ checksum "requirements.txt" }}
          paths:
            - ~/.cache/pip

  coverage:
    parameters:
      codecov_report: 
        description: Path to the coverage report file
        type: string
        default: "codecov.xml"
    docker:
      - image: codecov/codecov@1.1.3
    steps:
      - codecov/upload:
          file: << parameters.codecov_report >>

  publish:
      executor: python/default
      steps:
        - checkout
        - python/dist
        - run: pip install twine && twine upload

workflows:
  main:
    # triggers:
    #   - schedule:
    #       cron: "0 0 * * 0"
    #       filters:
    #         branches:
    #           only:
    #             - master
    #             - dev
    jobs:
      - lint
      - build:
          requires:
            - lint

      - test:
          name: test_py36
          requires:
            - build
          executor: python36
          toxenv: "coverage-py36"
      - test:
          name: test_py37
          requires:
            - build
          executor: python37
          toxenv: "coverage-py37"
      - test:
          name: test_py38
          requires:
            - build
          executor: python38
          toxenv: "coverage-py38"
          persist_coverage: true
      - coverage:
          requires:
            - build
            - test_py36
            - test_py37
            - test_py38
      - hold:
          type: approval
          requires:
            - build
          filters:
            tags:
              only: /[0-9]+(\.[0-9]+)*/
            branches:
              ignore: /.*/
      - publish:
          requires:
            - hold
