.. MetaPathways documentation master file, created by
   sphinx-quickstart on Wed Sep 23 13:09:29 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


MetaPathways
============

.. toctree::
   :maxdepth: 2
   :caption: Documentation

   overview
   quick_start
   install
   usage

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

.. Contact
.. =======

.. :ref:`contact`

