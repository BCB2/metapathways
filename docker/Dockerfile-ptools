FROM condaforge/mambaforge as build-env
# https://mamba.readthedocs.io/en/latest/user_guide/mamba.html
MAINTAINER Ryan J. McLaughlin, Tony Liu, & Tomer Altman, Altman Analytics LLC

### EXAMPLES ###

### Build dev branch
# sudo docker build --network=host -t metapathways:dev .

### Build test branch
# sudo docker build --build-arg git_branch=test --network=host -t metapathways:test .

################

# Create the environment:
# packages from `metapathways-install-deps.sh` are included in the yml
COPY ./docker/conda_env.yml /opt/
RUN mamba env create --no-default-packages -f /opt/conda_env.yml
ENV PATH /opt/conda/envs/metapathways/bin:$PATH

# Install MetaPathways and dependencies
# use the local install so local builds are up to date
# !! remember to run `make create-package` at least once !!
COPY ./dist/*.gz /opt/pip_dist/
RUN pip3 install /opt/pip_dist/*.gz
RUN pip3 install git+https://github.com/hallamlab/MAGSplitter.git@main
RUN echo "Make sure MetaPathways is installed:"
RUN MetaPathways -h
RUN echo "Make sure MAGsplitter is installed:"
RUN magsplitter -h

# Singularity uses tini, but raises warnings
# we set it up here correctly for singularity
ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini

## We do some umask munging to avoid having to use chmod later on,
## as it is painfully slow on large directores in Docker.
RUN old_umask=`umask` && \
    umask 0000 && \
    umask $old_umask

# move to clean execution environment
# which includes dependencies like wget & libtinfo6
FROM ubuntu

# Install Ptools and all dependencies
ENV DEBIAN_FRONTEND=noninteractive \
    DISPLAY=:99 \
    INSTALLER_DIR=/opt/bin \
    INSTALL_EXE=docker/ptools-container/pathway-tools-27.0-linux-64-tier1-install

## Ubuntu installs:
RUN apt-get update && \
    apt-get install -y xterm openssl inetutils-ping libxml2 xvfb libxm4 && \
    apt-get install -y emacs-nox less wget procps libssl-dev

## PTools installs:
COPY ./docker/ptools-container/ptools-init.dat /opt/sandbox/ptools-init.dat
COPY ./docker/ptools-container/install-pathway-tools.sh /tmp/bin/install-pathway-tools.sh
COPY ./docker/ptools-container/run-pathway-tools.sh /tmp/bin/run-pathway-tools.sh
COPY $INSTALL_EXE $INSTALLER_DIR/$INSTALL_EXE

RUN /tmp/bin/install-pathway-tools.sh
## Clean up the installer, which is no longer needed:
RUN rm $INSTALLER_DIR/$INSTALL_EXE

ENV PATH=$PATH:/opt/pathway-tools:/opt/bin:/opt/conda/bin/
RUN pathway-tools -lisp -eval "(exit)"

COPY --from=build-env /opt/conda/envs/metapathways /opt/conda/envs/metapathways
COPY --from=build-env /tini /tini

## We do some umask munging to avoid having to use chmod later on,
## as it is painfully slow on large directores in Docker.
RUN old_umask=`umask` && \
    umask 0000 && \
    umask $old_umask

ENV PATH /opt/conda/envs/metapathways/bin:$PATH

# singularity doesn't use the -s flag, and that causes warnings
ENTRYPOINT ["/tini", "-s", "--"]
